#!/usr/bin/env python3

h, v, a = 0, 0, 0

for i in [x.strip() for x in open("day2.txt").readlines()]:
    match i.split():
        case ['down', num]:
            a += int(num)
        case ['up', num]:
            a -= int(num)
        case ['forward', num]:
            h += int(num)
            v += a * int(num)

print(h * v)
