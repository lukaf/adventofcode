package day7

import (
	"log"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseMainBag(t *testing.T) {
	tests := []struct {
		input string
		bag   string
	}{
		{
			"bright blue bags contain 1 pale beige bag, 1 light teal bag.",
			"bright blue",
		},
		{
			"pale lavender bags contain 5 muted red bags, 3 dark teal bags, 3 faded black bags, 1 dim fuchsia bag.",
			"pale lavender",
		},
		{
			"drab red bags contain no other bags.",
			"drab red",
		},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			bag := parseMainBag(test.input)
			assert.Equal(t, test.bag, bag, "error parsing main bag")
		})
	}
}

func TestParseExtraBags(t *testing.T) {
	tests := []struct {
		input string
		bags  []string
	}{
		{
			"bright blue bags contain 1 pale beige bag, 1 light teal bag.",
			[]string{"1 pale beige", "1 light teal"},
		},
		{
			"pale lavender bags contain 5 muted red bags, 3 dark teal bags, 3 faded black bags, 1 dim fuchsia bag.",
			[]string{"5 muted red", "3 dark teal", "3 faded black", "1 dim fuchsia"},
		},
		{
			"drab red bags contain no other bags.",
			[]string{},
		},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			bags := parseExtraBags(test.input)
			assert.Equal(t, test.bags, bags, "error parsing extra bags")
		})
	}
}

func TestPart1Example(t *testing.T) {

	input := `light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.`

	result := 4

	f := strings.NewReader(input)
	bags := parse(f)

	assert.Equal(t, result, part1(bags))

}

func TestPart1(t *testing.T) {
	want := 224

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	bags := parse(f)

	assert.Equal(t, want, part1(bags))
}

func TestPart2Example(t *testing.T) {
	input := `shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.`

	result := 126

	f := strings.NewReader(input)
	bags := parse(f)

	assert.Equal(t, result, part2(bags))
}

func TestPart2(t *testing.T) {
	want := 1488

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	bags := parse(f)

	assert.Equal(t, want, part2(bags))
}
