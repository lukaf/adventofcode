package day5

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMove(t *testing.T) {
	tests := []struct {
		direction rune
		minmax    []int
		result    []int
	}{
		{
			'F',
			[]int{0, 9},
			[]int{0, 4},
		},
		{
			'B',
			[]int{0, 9},
			[]int{5, 9},
		},
		{
			'L',
			[]int{0, 9},
			[]int{0, 4},
		},
		{
			'R',
			[]int{0, 9},
			[]int{5, 9},
		},
	}

	for i, test := range tests {
		t.Run(fmt.Sprintf("%v", i), func(t *testing.T) {
			if r := move(test.minmax, test.direction); !reflect.DeepEqual(r, test.result) {
				t.Fatalf("wrong output, got %v, want %v\n",
					r,
					test.result,
				)
			}
		})
	}
}

func TestTraverse(t *testing.T) {
	tests := []struct {
		input  string
		max    int
		result int
	}{
		{"FBFBBFF", 127, 44},
		{"RLR", 7, 5},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			if r := parse(test.input, test.max); r != test.result {
				t.Fatalf("wrong output, got %v, want %v\n",
					r,
					test.result,
				)
			}
		})
	}
}

func TestParseRowSeat(t *testing.T) {
	tests := []struct {
		input string
		row   string
		seat  string
	}{
		{"BFFFBBFRRR", "BFFFBBF", "RRR"},
		{"FFFBBBFRRR", "FFFBBBF", "RRR"},
		{"BBFFBBFRLL", "BBFFBBF", "RLL"},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			out := parseRowSeat(test.input)
			if out[0] != test.row {
				t.Fatalf("wrong row, got %v, want %v\n",
					out[0],
					test.row,
				)
			}

			if out[1] != test.seat {
				t.Fatalf("wrong seat, got %v, want %v\n",
					out[1],
					test.seat,
				)
			}
		})
	}
}

func TestTicketId(t *testing.T) {
	tests := []struct {
		input string
		id    int
	}{
		{"BFFFBBFRRR", 567},
		{"FFFBBBFRRR", 119},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			if id := ticketId(test.input); id != test.id {
				t.Fatalf("wrong id, got %v, want %v\n",
					id,
					test.id,
				)
			}
		})
	}
}

func TextMaxInt(t *testing.T) {
	tests := []struct {
		input []int
		max   int
	}{
		{
			[]int{1, 2, 3},
			3,
		},
		{
			[]int{4, 5, 6},
			6,
		}, {
			[]int{1, 2, 3, 4, 5, 6, 7},
			7,
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%v", test.input), func(t *testing.T) {
			if out := maxInt(test.input); out != test.max {
				t.Fatalf("wrong max, got %v, want %v\n",
					out,
					test.max,
				)
			}
		})
	}
}

func TestFindMissingId(t *testing.T) {
	tests := []struct {
		input   []int
		missing int
	}{
		{
			[]int{1, 2, 3, 5},
			4,
		},
		{
			[]int{50, 51, 52, 54, 55, 56},
			53,
		},
		{
			[]int{12, 11, 14, 16, 10, 15},
			13,
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%v", test.input), func(t *testing.T) {
			if missing := findMissingId(test.input); missing != test.missing {
				t.Fatalf("wrong id found, got %v, want %v\n",
					missing,
					test.missing,
				)
			}
		})
	}
}

func TestPart1(t *testing.T) {
	want := 965

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	ids := make([]int, 0)

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()

		id := ticketId(line)
		ids = append(ids, id)

	}

	assert.Equal(t, want, maxInt(ids))
}

func TestPart2(t *testing.T) {
	want := 524

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	ids := make([]int, 0)

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()

		id := ticketId(line)
		ids = append(ids, id)

	}

	assert.Equal(t, want, findMissingId(ids))
}
