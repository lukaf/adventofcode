package day4

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func parsePassports(input io.Reader) map[int][]string {
	scanner := bufio.NewScanner(input)

	out := make(map[int][]string)

	passportCount := 0
	for scanner.Scan() {
		line := scanner.Text()

		if len(line) == 0 {
			passportCount += 1
			continue
		}

		fields := strings.Split(line, " ")

		if _, ok := out[passportCount]; !ok {
			out[passportCount] = []string{}
		}

		out[passportCount] = append(out[passportCount], fields...)
	}

	return out
}

func containsString(items []string, item string) bool {
	for _, i := range items {
		if i == item {
			return true
		}
	}

	return false
}

func containsField(fields []string, field string) bool {
	for _, f := range fields {
		if strings.HasPrefix(f, fmt.Sprintf("%s:", field)) {
			return true
		}
	}

	return false
}

func isPassportValid(fields []string, validationFields []string) bool {
	for _, field := range validationFields {
		if !containsField(fields, field) {
			return false
		}
	}

	return true
}

func inRange(r []int, number int) bool {
	return number >= r[0] && number <= r[1]
}

func validYear(field string) bool {
	year := parseNumber(parseValueFromField(field))

	prefix := field[0:3]
	switch prefix {
	case "byr":
		return inRange([]int{1920, 2002}, year)
	case "iyr":
		return inRange([]int{2010, 2020}, year)
	case "eyr":
		return inRange([]int{2020, 2030}, year)
	default:
		return false
	}
}

func validCM(height int) bool {
	return inRange([]int{150, 193}, height)
}

func validIN(height int) bool {
	return inRange([]int{59, 76}, height)
}

func validHCL(field string) bool {
	colour := parseValueFromField(field)
	r := regexp.MustCompile("#[a-f0-9]{6}")
	return r.MatchString(colour)
}

func validECL(field string) bool {
	validColours := []string{
		"amb",
		"blu",
		"brn",
		"gry",
		"grn",
		"hzl",
		"oth",
	}

	colour := parseValueFromField(field)
	return containsString(validColours, colour)
}

func validPID(field string) bool {
	value := parseValueFromField(field)
	if parseNumber(value) == -1 {
		return false
	}

	return len(value) == 9
}

func parseValueFromField(field string) string {
	components := strings.Split(field, ":")
	if len(components) != 2 {
		return ""
	}
	return components[1]

}

func parseNumber(number string) int {
	n, err := strconv.Atoi(number)
	if err != nil {
		return -1
	}
	return n
}

func validHeight(field string) bool {
	value := parseValueFromField(field)

	number, err := strconv.Atoi(value[0 : len(value)-2])
	if err != nil {
		log.Println(err)
		return false
	}

	unit := value[len(value)-2:]
	valid := false
	switch unit {
	case "cm":
		valid = validCM(number)

	case "in":
		valid = validIN(number)
	}

	return valid
}

func isPassportValidExtended(fields []string) bool {
	valid := false

	for _, field := range fields {
		fieldPrefix := field[0:3]
		switch fieldPrefix {
		case "byr":
			valid = validYear(field)
		case "iyr":
			valid = validYear(field)
		case "eyr":
			valid = validYear(field)
		case "hgt":
			valid = validHeight(field)
		case "hcl":
			valid = validHCL(field)
		case "ecl":
			valid = validECL(field)
		case "pid":
			valid = validPID(field)
		default:
			continue
		}

		if !valid {
			return false
		}
	}

	return valid
}
