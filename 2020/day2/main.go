package day2

import (
	"log"
	"strconv"
	"strings"
)

func parsePolicy(input string) map[string][]int {
	policy := strings.Split(input, ":")[0]

	policyItems := strings.Split(policy, " ")

	char := policyItems[1]
	positions := strings.Split(policyItems[0], "-")

	min, err := strconv.Atoi(positions[0])
	if err != nil {
		log.Fatal(err)
	}

	max, err := strconv.Atoi(positions[1])
	if err != nil {
		log.Fatal(err)
	}

	return map[string][]int{
		char: []int{min, max},
	}
}

func parsePassword(input string) map[string]int {
	password := strings.Split(input, ":")[1]
	password = strings.TrimSpace(password)

	output := make(map[string]int)
	for _, char := range password {
		c := string(char)
		if _, ok := output[c]; !ok {
			output[c] = 0
		}
		output[c]++
	}

	return output
}

func isPasswordValid(password map[string]int, policy map[string][]int) bool {
	var char string
	for key, _ := range policy {
		char = key
	}

	charCount, ok := password[char]
	if !ok {
		return false
	}

	min := policy[char][0]
	max := policy[char][1]

	if charCount >= min && charCount <= max {
		return true
	}

	return false
}

func isPasswordValid2(password string, policy map[string][]int) bool {
	var char string
	for key, _ := range policy {
		char = key
	}

	pos1 := policy[char][0]
	pos2 := policy[char][1]

	// adjust for zero based index slices
	char1 := string(password[pos1-1])
	char2 := string(password[pos2-1])

	if len(password) < pos2 && char1 == char {
		return true
	}

	if char1 == char && char2 == char {
		return false
	}

	if char1 == char || char2 == char {
		return true
	}

	return false
}

func extractPassword(input string) string {
	password := strings.Split(input, ":")[1]
	return strings.TrimSpace(password)
}
