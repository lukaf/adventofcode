#!/usr/bin/env python3.10

import collections

data = [x.strip() for x in open("day3.txt").readlines()]

binary_width = len(data[0])

counters = []
for i in range(binary_width):
    counters.append(collections.Counter([x[i] for x in data]))


gamma = int(''.join([x.most_common()[0][0] for x in counters]), 2)
epsilon = ~gamma % (1 << binary_width)

print(gamma * epsilon)
