package day5

import (
	"sort"
)

func findMissingId(input []int) int {
	sort.Ints(input)
	for i, id := range input {
		if len(input) > i+1 &&
			input[i+1] != id+1 && input[i+1] == id+2 {
			return id + 1
		}
	}

	return -1
}

func maxInt(input []int) int {
	x := 0
	for _, i := range input {
		if i > x {
			x = i
		}
	}
	return x
}

func move(input []int, direction rune) []int {
	switch direction {
	case 'F', 'L':
		return func(i []int) []int {
			base := i[1] - i[0]
			if base == 1 {
				return []int{i[0], i[0]}
			}

			return []int{
				i[0],
				base/2 + i[0],
			}
		}(input)

	case 'B', 'R':
		return func(i []int) []int {
			base := i[1] - i[0]
			if base == 1 {
				return []int{i[1], i[1]}
			}

			return []int{
				base/2 + i[0] + 1, // +1 because we start with 0
				i[1],
			}
		}(input)

	default:
		return []int{-1, -1}
	}
}

func parse(input string, max int) int {
	step := []int{0, max}

	for _, s := range input {
		step = move(step, s)
	}

	return step[0]
}

func parseRowSeat(input string) []string {
	return []string{
		input[:len(input)-3],
		input[len(input)-3 : len(input)],
	}
}

func ticketId(input string) int {
	ticket := parseRowSeat(input)

	return (parse(ticket[0], 127) * 8) + parse(ticket[1], 7)
}
