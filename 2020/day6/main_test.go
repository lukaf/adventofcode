package day6

import (
	"log"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAnyoneAnswers(t *testing.T) {
	input := `abc

a
b
c

ab
ac

a
a
a
a

b`
	want := 11

	f := strings.NewReader(input)
	if got := countAnyoneAnswers(f); got != want {
		t.Errorf("wrong answer count, got %v, want %v\n",
			got,
			want,
		)
	}
}

func TestEveryoneAnswers(t *testing.T) {
	input := `abc

a
b
c

ab
ac

a
a
a
a

b`
	want := 6

	f := strings.NewReader(input)
	if got := countEveryoneAnswers(f); got != want {
		t.Errorf("wrong answer count, got %v, want %v\n",
			got,
			want,
		)
	}
}

func TestPart1(t *testing.T) {
	want := 6532

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	assert.Equal(t, want, countAnyoneAnswers(f))
}

func TestPart2(t *testing.T) {
	want := 3427

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	assert.Equal(t, want, countEveryoneAnswers(f))
}
