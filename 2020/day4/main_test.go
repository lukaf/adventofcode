package day4

import (
	"fmt"
	"log"
	"os"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var input = `ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in`

var passports = map[int][]string{
	0: []string{"ecl:gry", "pid:860033327", "eyr:2020", "hcl:#fffffd", "byr:1937", "iyr:2017", "cid:147", "hgt:183cm"},
	1: []string{"iyr:2013", "ecl:amb", "cid:350", "eyr:2023", "pid:028048884", "hcl:#cfa07d", "byr:1929"},
	2: []string{"hcl:#ae17e1", "iyr:2013", "eyr:2024", "ecl:brn", "pid:760753108", "byr:1931", "hgt:179cm"},
	3: []string{"hcl:#cfa07d", "eyr:2025", "pid:166559648", "iyr:2011", "ecl:brn", "hgt:59in"},
}

var passportFields = []string{
	"byr",
	"iyr",
	"eyr",
	"hgt",
	"hcl",
	"ecl",
	"pid",
}

var passportFieldsOptional = []string{"cid"}

func TestParsePassports(t *testing.T) {
	f := strings.NewReader(input)
	parsed := parsePassports(f)

	if !reflect.DeepEqual(parsed, passports) {
		t.Fatalf("failed parsing passports, got %v, want %v\n",
			parsed,
			passports,
		)
	}
}

func TestContainsString(t *testing.T) {
	tests := []struct {
		items    []string
		item     string
		contains bool
	}{
		{
			[]string{"one", "two", "three"},
			"one",
			true,
		},
		{
			[]string{"one", "two", "three"},
			"four",
			false,
		},
		{
			[]string{},
			"one",
			false,
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%v", test.items), func(t *testing.T) {
			if contains := containsString(test.items, test.item); contains != test.contains {
				t.Errorf("wrong answer to does %v contain %v: got %v, want %v\n",
					test.items,
					test.item,
					contains,
					test.contains,
				)
			}
		})
	}
}

func TestIsPassportValid(t *testing.T) {
	validatedPassports := map[int]bool{
		0: true,
		1: false,
		2: true,
		3: false,
	}

	validationFields := passportFields

	for passportId, fields := range passports {
		if valid := isPassportValid(fields, validationFields); valid != validatedPassports[passportId] {
			t.Fatalf("wrongly validated passport with Id %d: got %v, want %v\n",
				passportId,
				valid,
				validatedPassports[passportId],
			)
		}
	}
}

func TestCountValidPassports(t *testing.T) {
	f := strings.NewReader(input)

	validationFields := passportFields

	valid := 0
	for _, fields := range parsePassports(f) {
		if isPassportValid(fields, validationFields) {
			valid += 1
		}
	}

	if valid != 2 {
		t.Fatalf("wrong number of valid passports, want %v, got %v\n",
			valid,
			2,
		)
	}
}

func TestInRange(t *testing.T) {
	tests := []struct {
		r       []int
		number  int
		inRange bool
	}{
		{
			[]int{1, 10},
			5,
			true,
		},
		{
			[]int{1, 10},
			100,
			false,
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%v", test.r), func(t *testing.T) {
			if ok := inRange(test.r, test.number); ok != test.inRange {
				t.Fatalf("wrong answer to is %d in range %v, got %v, want %v\n",
					test.number,
					test.r,
					ok,
					test.inRange,
				)
			}
		})
	}
}

func TestValidYear(t *testing.T) {
	tests := []struct {
		year  string
		valid bool
	}{
		{"byr:1", false},
		{"byr:1920", true},
		{"byr:1919", false},
		{"byr:2002", true},
		{"byr:2003", false},
		{"iyr:1", false},
		{"iyr:2010", true},
		{"iyr:2009", false},
		{"iyr:2020", true},
		{"iyr:2021", false},
		{"eyr:1", false},
		{"eyr:2020", true},
		{"eyr:2019", false},
		{"eyr:2030", true},
		{"eyr:2031", false},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%v", test.year), func(t *testing.T) {
			if valid := validYear(test.year); valid != test.valid {
				t.Errorf("invalid BYR validation, got %v, want %v\n",
					valid,
					test.valid,
				)
			}
		})
	}
}

func TestValidCM(t *testing.T) {
	tests := []struct {
		year  int
		valid bool
	}{
		{1, false},
		{150, true},
		{149, false},
		{193, true},
		{194, false},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%v", test.year), func(t *testing.T) {
			if valid := validCM(test.year); valid != test.valid {
				t.Errorf("invalid height in CM validation, got %v, want %v\n",
					valid,
					test.valid,
				)
			}
		})
	}
}

func TestValidIN(t *testing.T) {
	tests := []struct {
		year  int
		valid bool
	}{
		{1, false},
		{59, true},
		{58, false},
		{76, true},
		{77, false},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%v", test.year), func(t *testing.T) {
			if valid := validIN(test.year); valid != test.valid {
				t.Errorf("invalid height in IN validation, got %v, want %v\n",
					valid,
					test.valid,
				)
			}
		})
	}
}

func TestValidHeight(t *testing.T) {
	tests := []struct {
		field string
		valid bool
	}{
		{"hgt:100in", false},
		{"hgt:60in", true},
		{"hgt:60", false},
		{"hgt:200cm", false},
		{"hgt:160cm", true},
		{"hgt:160", false},
	}

	for _, test := range tests {
		t.Run(test.field, func(t *testing.T) {
			if ok := validHeight(test.field); ok != test.valid {
				t.Errorf("invalid height validation, got %v, want %v\n",
					ok,
					test.valid,
				)
			}
		})
	}
}

func TestValidHCL(t *testing.T) {
	tests := []struct {
		colour string
		valid  bool
	}{
		{"hcl:#123abc", true},
		{"hcl:#123abz", false},
		{"hcl:123abc", false},
	}

	for _, test := range tests {
		t.Run(test.colour, func(t *testing.T) {
			if ok := validHCL(test.colour); ok != test.valid {
				t.Fatalf("invalid colour validation, got %v, want %v\n",
					ok,
					test.valid,
				)
			}
		})
	}
}

func TestValidECL(t *testing.T) {
	tests := []struct {
		colour string
		valid  bool
	}{
		{"ecl:brn", true},
		{"ecl:wat", false},
	}

	for _, test := range tests {
		t.Run(test.colour, func(t *testing.T) {
			if ok := validECL(test.colour); ok != test.valid {
				t.Fatalf("invalid color validation, got %v, want %v\n",
					ok,
					test.valid,
				)
			}
		})
	}
}

func TestValidPID(t *testing.T) {
	tests := []struct {
		pid   string
		valid bool
	}{
		{"pid:000000001", true},
		{"pid:0123456789", false},
	}

	for _, test := range tests {
		t.Run(test.pid, func(t *testing.T) {
			if ok := validPID(test.pid); ok != test.valid {
				t.Fatalf("invalid password validation, want %v, got %v\n",
					ok,
					test.valid,
				)
			}
		})
	}
}

func TestContainsField(t *testing.T) {
	tests := []struct {
		fields   []string
		field    string
		contains bool
	}{
		{
			[]string{"abc:123", "def:#123"},
			"abc",
			true,
		},
		{
			[]string{"efg:#123", "hij:abc"},
			"abc",
			false,
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%v", test.fields), func(t *testing.T) {
			if ok := containsField(test.fields, test.field); ok != test.contains {
				t.Fatalf("wrong field detection, got %v, want %v\n",
					ok,
					test.contains,
				)
			}
		})
	}
}

func TestParseValueFromField(t *testing.T) {
	tests := []struct {
		field  string
		number string
	}{
		{"abc:123", "123"},
		{"abc:abc", "abc"},
		{"abcabc", ""},
		{"abc:00001", "00001"},
	}

	for _, test := range tests {
		t.Run(test.field, func(t *testing.T) {
			if out := parseValueFromField(test.field); out != test.number {
				t.Errorf("failed parsing number, got %v, want %v\n",
					out,
					test.number,
				)
			}
		})
	}
}

func TestParseNumber(t *testing.T) {
	tests := []struct {
		input  string
		number int
	}{
		{"123", 123},
		{"abc", -1},
		{"00001", 1},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			if n := parseNumber(test.input); n != test.number {
				t.Errorf("failed parsing number, got %v, want %v\n",
					n,
					test.number,
				)
			}
		})
	}
}

func TestIsPassportValidExtended(t *testing.T) {
	input := `eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007

pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719`

	validationFields := passportFields

	f := strings.NewReader(input)
	passports := parsePassports(f)

	validPassports := 0
	for _, fields := range passports {
		if !isPassportValid(fields, validationFields) {
			continue
		}

		if isPassportValidExtended(fields) {
			validPassports += 1
		}
	}

	if validPassports != 4 {
		t.Errorf("failed to determine valid passports, got %v, want %v\n",
			validPassports,
			4,
		)
	}
}

func TestPart1(t *testing.T) {
	want := 190

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	validationFields := []string{
		"byr",
		"iyr",
		"eyr",
		"hgt",
		"hcl",
		"ecl",
		"pid",
	}

	valid := 0
	for _, fields := range parsePassports(f) {
		if isPassportValid(fields, validationFields) {
			valid += 1
		}
	}

	assert.Equal(t, want, valid)
}

func TestPart2(t *testing.T) {
	want := 121

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	validationFields := []string{
		"byr",
		"iyr",
		"eyr",
		"hgt",
		"hcl",
		"ecl",
		"pid",
	}

	valid := 0
	for _, fields := range parsePassports(f) {
		if !isPassportValid(fields, validationFields) {
			continue
		}

		if isPassportValidExtended(fields) {
			valid += 1
		}
	}

	assert.Equal(t, want, valid)
}
