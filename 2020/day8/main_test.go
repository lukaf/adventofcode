package day8

import (
	"log"
	"os"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var input = `nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6`

func TestParse(t *testing.T) {
	f := strings.NewReader(input)

	ops := []Operation{
		{"nop", +0},  // 0
		{"acc", +1},  // 1
		{"jmp", +4},  // 2
		{"acc", +3},  // 3
		{"jmp", -3},  // 4
		{"acc", -99}, // 5
		{"acc", +1},  // 6
		{"jmp", -4},  // 7
		{"acc", +6},  // 8
	}

	parsedOps := parse(f)

	assert.Equal(t, ops, parsedOps)
}

func TestCompute(t *testing.T) {
	f := strings.NewReader(input)

	ops := parse(f)
	computer := &Computer{
		Operations: ops,
		Executed:   make(map[int]bool),
	}

	tests := []struct {
		in  int
		out int
	}{
		{0, 1},
		{1, 2},
		{2, 6},
		{6, 7},
		{3, 4},
		{4, 1},
	}

	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			assert.Equal(t, test.out, computer.compute(test.in))
		})
	}

	assert.Equal(t, 5, computer.Accumulator)
	assert.Equal(t, false, computer.Executed[8])
	assert.Equal(t, true, computer.Executed[1])
	assert.Equal(t, len(tests), len(computer.Executed))

}

func TestPart1Example(t *testing.T) {
	f := strings.NewReader(input)

	ops := parse(f)
	computer := &Computer{
		Operations: ops,
		Executed:   make(map[int]bool),
	}

	assert.Equal(t, 5, part1(computer))
}

func TestPart1(t *testing.T) {
	want := 1137

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	ops := parse(f)

	assert.Equal(
		t,
		want,
		part1(&Computer{
			Operations: ops,
			Executed:   make(map[int]bool),
		}),
	)
}

func TestPart2Example(t *testing.T) {
	f := strings.NewReader(input)

	ops := parse(f)
	computer := &Computer{
		Operations: ops,
		Executed:   make(map[int]bool),
	}

	assert.Equal(t, 8, part2(computer))
}

func TestPart2(t *testing.T) {
	want := 1125

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	ops := parse(f)

	assert.Equal(
		t,
		want,
		part2(&Computer{
			Operations: ops,
			Executed:   make(map[int]bool),
		}),
	)
}
