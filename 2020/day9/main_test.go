package day9

import (
	"log"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenerateSums(t *testing.T) {
	input := []int{1, 2, 3, 4}

	want := map[int]bool{
		3: true,
		4: true,
		5: true,
		6: true,
		7: true,
	}

	out := generateSums(input)

	assert.Equal(t, want, out)
}

func TestParse(t *testing.T) {
	input := `1
2
3
4
5
6
7
8`

	want := []int{1, 2, 3, 4, 5, 6, 7, 8}

	f := strings.NewReader(input)
	assert.Equal(t, want, parse(f))
}

func TestPart1Example(t *testing.T) {
	input := `35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576`

	want := 127

	f := strings.NewReader(input)

	nums := parse(f)
	num, _ := part1(nums, 5)
	assert.Equal(t, want, num)
}

func TestSum(t *testing.T) {
	tests := []struct {
		input []int
		want  int
	}{
		{[]int{1, 2, 3, 4}, 10},
		{[]int{5, 5, 5}, 15},
		{[]int{}, 0},
	}

	for _, test := range tests {
		assert.Equal(t, test.want, sum(test.input))
	}
}

func TestPart2Example(t *testing.T) {
	input := `35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576
`
	want := 62

	f := strings.NewReader(input)
	nums := parse(f)
	_, index := part1(nums, 5)
	assert.Equal(t, want, part2(nums, index))

}

func TestPart1(t *testing.T) {
	want := 756008079

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	nums := parse(f)

	invalidNum, _ := part1(nums, 25)
	assert.Equal(t, want, invalidNum)
}

func TestPart2(t *testing.T) {
	want := 93727241

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	nums := parse(f)

	_, index := part1(nums, 25)
	assert.Equal(t, want, part2(nums, index))
}
