package day3

import (
	"bufio"
	"io"
)

func parseInput(input io.Reader) map[int][]int {
	scanner := bufio.NewScanner(input)
	lineCount := 0

	output := make(map[int][]int)

	for scanner.Scan() {
		trees := make([]int, 0)

		for index, char := range scanner.Text() {
			if char == '#' {
				trees = append(trees, index)
			}
		}

		output[lineCount] = trees
		lineCount += 1
	}

	return output
}

func inputWidth(input io.Reader) int {
	scanner := bufio.NewScanner(input)
	scanner.Scan()
	return len(scanner.Text())
}

func getLocation(line, multiplier int) int {
	if line == 0 {
		return -1
	}

	return line*multiplier + 1
}

func currentLocation(line int) int {
	return getLocation(line, 3)
}

func currentLocation1x1(line int) int {
	return getLocation(line, 1)
}

func currentLocation3x1(line int) int {
	return currentLocation(line)
}

func currentLocation5x1(line int) int {
	return getLocation(line, 5)
}

func currentLocation7x1(line int) int {
	return getLocation(line, 7)
}

func currentLocation1x2(line int) int {
	if line == 0 {
		return -1
	}

	if line%2 == 0 {
		return line/2 + 1
	}

	return -1
}

func locationFold(location int, width int) int {
	foldedLocation := location % width
	if foldedLocation == 0 {
		return width
	}

	return foldedLocation
}

func containsInt(items []int, item int) bool {
	for _, i := range items {
		if i == item {
			return true
		}
	}

	return false
}

func treeCounter(input map[int][]int, width int, f func(int) int) int {
	trees := 0
	for line, treeLocations := range input {
		location := f(line)
		location = locationFold(location, width)
		if location == -1 {
			continue
		}

		if containsInt(treeLocations, location-1) {
			trees += 1
		}
	}

	return trees
}
