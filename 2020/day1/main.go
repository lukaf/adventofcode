package day1

import (
	"bufio"
	"io"
	"strconv"
)

const input = "input"

func pairSum2020(items []int) int {
	for _, item1 := range items {
		for _, item2 := range items {
			if item1+item2 == 2020 {
				return item1 * item2
			}
		}
	}

	return 0
}

func tripleSum2020(input []int) int {
	for _, item1 := range input {
		for _, item2 := range input {
			for _, item3 := range input {
				if item1+item2+item3 == 2020 {
					return item1 * item2 * item3
				}
			}
		}
	}

	return 0
}

func parse(input io.Reader) []int {
	nums := make([]int, 0)

	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		line := scanner.Text()

		i, err := strconv.Atoi(line)
		if err != nil {
			panic(err)
		}

		nums = append(nums, i)
	}

	return nums
}
