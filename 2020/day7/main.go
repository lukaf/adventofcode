package day7

import (
	"bufio"
	"io"
	"regexp"
	"strconv"
	"strings"
)

type Node struct {
	Name  string
	Edges []*Edge
}

type Edge struct {
	Count int
	Node  *Node
}

func part1(bags map[string]*Node) int {
	count := 0
	for _, bag := range bags {
		if bag.Name == "shiny gold" {
			continue
		}

		if holdsShinyGoldBag(bag) {
			count += 1
		}
	}

	return count
}

func holdsShinyGoldBag(node *Node) bool {
	if node.Name == "shiny gold" {
		return true
	}

	for _, e := range node.Edges {
		if holdsShinyGoldBag(e.Node) {
			return true
		}
	}

	return false
}

func part2(bags map[string]*Node) int {
	return countBags(bags["shiny gold"])
}

func countBags(node *Node) int {
	count := 0

	for _, e := range node.Edges {
		count += e.Count + e.Count*countBags(e.Node)
	}

	return count
}

func parse(input io.Reader) map[string]*Node {

	bags := make(map[string]*Node)

	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		line := scanner.Text()

		mainBag := parseMainBag(line)
		extraBags := parseExtraBags(line)

		if bags[mainBag] == nil {
			bags[mainBag] = &Node{
				Name:  mainBag,
				Edges: make([]*Edge, 0),
			}
		}

		for _, bag := range extraBags {
			split := strings.SplitN(bag, " ", 2)
			count, err := strconv.Atoi(split[0])
			if err != nil {
				panic(err)
			}

			name := split[1]

			if bags[name] == nil {
				bags[name] = &Node{
					Name:  name,
					Edges: make([]*Edge, 0),
				}
			}

			edge := &Edge{
				Count: count,
				Node:  bags[name],
			}

			bags[mainBag].Edges = append(bags[mainBag].Edges, edge)
		}
	}

	return bags
}

func parseMainBag(line string) string {
	r := regexp.MustCompile(`^(\w+ \w+)`)
	return r.FindString(line)
}

func parseExtraBags(line string) []string {
	r := regexp.MustCompile(`(\d+ (\w+ \w+))+`)

	bags := make([]string, 0)
	for _, bag := range r.FindAllStringSubmatch(line, -1) {
		bags = append(bags, bag[1])
	}

	return bags
}
