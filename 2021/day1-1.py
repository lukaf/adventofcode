#!/usr/bin/env python3

i = [int(x) for x in open("day1.txt").readlines()]

print(len(list(filter(lambda x: x[0] < x[1], zip(i[:-1], i[1:])))))
