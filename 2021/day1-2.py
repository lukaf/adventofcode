#!/usr/bin/env python3

i = [int(x) for x in open("day1.txt").readlines()]

l = [sum(x) for x in zip(i[:-2], i[1:-1], i[2:])]

print(len(list(filter(lambda x: x[0] < x[1], zip(l[:-1], l[1:])))))
