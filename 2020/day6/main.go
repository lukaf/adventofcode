package day6

import (
	"bufio"
	"io"
)

func countAnyoneAnswers(input io.Reader) int {
	scanner := bufio.NewScanner(input)

	groups := []map[rune]struct{}{}
	answers := make(map[rune]struct{})

	for scanner.Scan() {
		line := scanner.Text()

		if len(line) == 0 {
			groups = append(groups, answers)
			answers = make(map[rune]struct{})

			continue
		}

		for _, r := range line {
			answers[r] = struct{}{}
		}
	}

	groups = append(groups, answers)

	answersCount := 0
	for _, a := range groups {
		answersCount += len(a)
	}

	return answersCount
}

func countEveryoneAnswers(input io.Reader) int {
	scanner := bufio.NewScanner(input)

	member := 0

	answers := make(map[rune]int)
	groups := make([]map[rune]int, 0)

	for scanner.Scan() {
		line := scanner.Text()

		if len(line) == 0 {
			// cleanup
			for k, v := range answers {
				if v != member {
					delete(answers, k)
				}
			}

			groups = append(groups, answers)

			answers = make(map[rune]int)
			member = 0

			continue
		}

		member += 1
		for _, c := range line {
			if _, ok := answers[c]; !ok {
				answers[c] = 0
			}

			answers[c] += 1
		}
	}

	// cleanup
	for k, v := range answers {
		if v != member {
			delete(answers, k)
		}
	}
	groups = append(groups, answers)

	answersCount := 0
	for _, a := range groups {
		answersCount += len(a)
	}

	return answersCount
}
