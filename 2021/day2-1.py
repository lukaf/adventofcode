#!/usr/bin/env python3

h, v = 0, 0

for i in [x.strip() for x in open("day2.txt").readlines()]:
    match i.split():
        case ['forward', num]:
            h += int(num)
        case ['down', num]:
            v += int(num)
        case ['up', num]:
            v -= int(num)

print(h * v)
