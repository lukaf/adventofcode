package day3

import (
	"fmt"
	"log"
	"os"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

// map of trees
var input = `..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#`

// all locations (per line of input) where toboggan will be
var locations = []int{
	-1, // first line of input is a starting point but we never stop there!
	4,
	7,
	10,
	13,
	16,
	19,
	22,
	25,
	28,
	31,
}

// parsed information about all tree locations
var trees = map[int][]int{
	0:  []int{2, 3},
	1:  []int{0, 4, 8},
	2:  []int{1, 6, 9},
	3:  []int{2, 4, 8, 10},
	4:  []int{1, 5, 6, 9},
	5:  []int{2, 4, 5},
	6:  []int{1, 3, 5, 10},
	7:  []int{1, 10},
	8:  []int{0, 2, 3, 7},
	9:  []int{0, 4, 5, 10},
	10: []int{1, 4, 8, 10},
}

func TestParseInput(t *testing.T) {
	f := strings.NewReader(input)
	parsed := parseInput(f)

	if len(parsed) == 0 {
		t.Fatal("got empty result")
	}

	for key, value := range parsed {
		if !reflect.DeepEqual(value, trees[key]) {
			t.Fatalf("failed parsing input line %d, want %v, got %v\n",
				key,
				trees[key],
				value,
			)
		}
	}
}

func TestInputWidth(t *testing.T) {
	tests := []struct {
		input string
		width int
	}{
		{"#", 1},
		{"##", 2},
		{"", 0},
		{"######", 6},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			f := strings.NewReader(test.input)
			if width := inputWidth(f); width != test.width {
				t.Fatalf("wrong width, got %v, want %v",
					width,
					test.width,
				)
			}
		})
	}
}

func TestCurrentLocation(t *testing.T) {
	for index, location := range locations {
		t.Run(fmt.Sprintf("line %d", index), func(t *testing.T) {
			if got := currentLocation(index); got != location {
				t.Fatalf("wrong current location, got %v, want %v\n",
					got,
					location,
				)
			}
		})
	}
}

func TestLocationFold(t *testing.T) {
	tests := []struct {
		line  int
		width int
		want  int
	}{
		{0, 11, -1},
		{1, 11, 4},
		{2, 11, 7},
		{3, 11, 10},
		{4, 11, 2},
		{5, 11, 5},
		// {6, 11, 4},
		// {7, 11, 2},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("line %d", test.line), func(t *testing.T) {
			location := currentLocation(test.line)
			if location := locationFold(location, test.width); location != test.want {
				t.Fatalf("wrong folded location, got %v, want %v\n",
					location,
					test.want,
				)
			}
		})
	}
}

func TestContainsInt(t *testing.T) {
	tests := []struct {
		items    []int
		item     int
		contains bool
	}{
		{
			[]int{1, 2, 3, 4},
			1,
			true,
		},
		{
			[]int{},
			1,
			false,
		},
		{
			[]int{1, 2, 3, 5},
			10,
			false,
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%v", test.items), func(t *testing.T) {
			if got := containsInt(test.items, test.item); got != test.contains {
				t.Fatalf("wrong answer to, does %v contain %v: %v\n",
					test.items,
					test.item,
					got,
				)
			}
		})
	}
}

func TestCountTrees(t *testing.T) {
	f := strings.NewReader(input)

	input := parseInput(f)
	f.Seek(0, 0)
	width := inputWidth(f)

	if width != 11 {
		t.Fatalf("wrong width, got %v, want 11\n", width)
	}

	if trees := treeCounter(input, width, currentLocation1x1); trees != 2 {
		t.Fatalf("1x1, got %v, want 2\n", trees)
	}

	if trees := treeCounter(input, width, currentLocation3x1); trees != 7 {
		t.Fatalf("3x1, got %v, want 7\n", trees)
	}

	if trees := treeCounter(input, width, currentLocation5x1); trees != 3 {
		t.Fatalf("5x1, got %v, want 3\n", trees)
	}

	if trees := treeCounter(input, width, currentLocation7x1); trees != 4 {
		t.Fatalf("7x1, got %v, want 4\n", trees)
	}

	if trees := treeCounter(input, width, currentLocation1x2); trees != 2 {
		t.Fatalf("1x2, got %v, want 2\n", trees)
	}

	trees := treeCounter(input, width, currentLocation1x1) *
		treeCounter(input, width, currentLocation3x1) *
		treeCounter(input, width, currentLocation5x1) *
		treeCounter(input, width, currentLocation7x1) *
		treeCounter(input, width, currentLocation1x2)

	if trees != 336 {
		t.Fatalf("wrong tree count, got %v, want 336\n", trees)
	}
}

func TestCurrentLocation1x1(t *testing.T) {
	input := []int{-1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	for index, position := range input {
		if got := currentLocation1x1(index); got != position {
			t.Fatalf("wrong 1 x 1 position, got %v, want %v\n",
				got,
				position,
			)
		}
	}
}

func TestCurrentLocation5x1(t *testing.T) {
	input := []int{-1, 6, 11, 16, 21, 26, 31, 36}
	for index, position := range input {
		if got := currentLocation5x1(index); got != position {
			t.Fatalf("wrong 5 x 1 position, got %v, want %v\n",
				got,
				position,
			)
		}
	}
}

func TestCurrentLocation7x1(t *testing.T) {
	input := []int{-1, 8, 15, 22, 29}
	for index, position := range input {
		if got := currentLocation7x1(index); got != position {
			t.Fatalf("wrong 7 x 1 position, got %v, want %v\n",
				got,
				position,
			)
		}
	}
}

func TestCurrentLocation1x2(t *testing.T) {
	input := []int{
		-1,
		-1,
		2,
		-1,
		3,
		-1,
		4,
		-1,
		5,
		-1,
		6,
	}
	for index, position := range input {
		if got := currentLocation1x2(index); got != position {
			t.Fatalf("wrong 1 x 2 position, got %v, want %v\n",
				got,
				position,
			)
		}
	}

}

func TestPart2(t *testing.T) {
	want := 1355323200

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}

	input := parseInput(f)
	f.Seek(0, 0)
	width := inputWidth(f)
	f.Close()

	trees := treeCounter(input, width, currentLocation1x1) *
		treeCounter(input, width, currentLocation3x1) *
		treeCounter(input, width, currentLocation5x1) *
		treeCounter(input, width, currentLocation7x1) *
		treeCounter(input, width, currentLocation1x2)

	assert.Equal(t, want, trees)
}

func TestPart1(t *testing.T) {
	want := 193

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}

	input := parseInput(f)
	f.Seek(0, 0)
	width := inputWidth(f)
	f.Close()

	trees := treeCounter(input, width, currentLocation3x1)

	assert.Equal(t, want, trees)
}
