package day2

import (
	"bufio"
	"log"
	"os"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParsePolicy(t *testing.T) {
	tests := []struct {
		input string
		want  map[string][]int
	}{
		{
			"1-3 a: abc",
			map[string][]int{"a": []int{1, 3}},
		},
		{
			"1-3 b: cdefg",
			map[string][]int{"b": []int{1, 3}},
		},
		{
			"2-9 c: ccccccc",
			map[string][]int{"c": []int{2, 9}},
		},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			if out := parsePolicy(test.input); !reflect.DeepEqual(out, test.want) {
				t.Errorf("failed parsing policy, want %v, got %v\n",
					test.want,
					out,
				)
			}
		})
	}
}

func TestParsePassword(t *testing.T) {
	tests := []struct {
		input string
		want  map[string]int
	}{
		{
			"1-3 a: abc",
			map[string]int{
				"a": 1,
				"b": 1,
				"c": 1,
			},
		},
		{
			"1-3 b: cdefg",
			map[string]int{
				"c": 1,
				"d": 1,
				"e": 1,
				"f": 1,
				"g": 1,
			},
		},
		{
			"2-9 c: ccccccc",
			map[string]int{
				"c": 7,
			},
		},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			if password := parsePassword(test.input); !reflect.DeepEqual(password, test.want) {
				t.Fatalf("failed parsing password, got %v, want %v\n",
					password,
					test.want,
				)
			}
		})
	}
}

func TestIsPasswordValid(t *testing.T) {
	tests := []struct {
		input string
		want  bool // is password valid?
	}{
		{
			"1-3 a: abc",
			true,
		},
		{
			"1-3 b: cdefg",
			false,
		},
		{
			"2-9 c: ccccccc",
			true,
		},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			policy := parsePolicy(test.input)
			password := parsePassword(test.input)

			if valid := isPasswordValid(password, policy); valid != test.want {
				t.Fatalf("failed validating password, got %v, want %v\n",
					valid,
					test.want,
				)
			}
		})
	}
}

func TestIsPasswordValid2(t *testing.T) {
	tests := []struct {
		input string
		want  bool // is password valid?
	}{
		{
			"1-3 a: abcde",
			true,
		},
		{
			"1-3 b: cdefg",
			false,
		},
		{
			"2-9 c: ccccccccc",
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			policy := parsePolicy(test.input)
			password := extractPassword(test.input)

			if valid := isPasswordValid2(password, policy); valid != test.want {
				t.Fatalf("failed validating password, got %v, want %v\n",
					valid,
					test.want,
				)
			}
		})
	}
}

func TestExtractPassword(t *testing.T) {
	tests := []struct {
		input string
		want  string
	}{
		{
			"1-3 a: abc",
			"abc",
		},
		{
			"1-3 b: cdefg",
			"cdefg",
		},
		{
			"2-9 c: ccccccc",
			"ccccccc",
		},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			if password := extractPassword(test.input); password != test.want {
				t.Fatalf("failed extracting password, got %v, want %v\n",
					password,
					test.want,
				)
			}
		})
	}
}

func TestPart1(t *testing.T) {
	want := 538

	f, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	validPasswords := 0
	invalidPasswords := 0

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()

		policy := parsePolicy(line)
		password := parsePassword(line)

		if isPasswordValid(password, policy) {
			validPasswords++
		} else {
			invalidPasswords++
		}
	}

	assert.Equal(t, want, validPasswords)
}

func TestPart2(t *testing.T) {
	want := 489

	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	validPasswords := 0
	invalidPasswords := 0

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()

		policy := parsePolicy(line)
		password := extractPassword(line)

		if isPasswordValid2(password, policy) {
			validPasswords++
		} else {
			invalidPasswords++
		}
	}

	assert.Equal(t, want, validPasswords)
}
