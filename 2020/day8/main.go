package day8

import (
	"bufio"
	"errors"
	"io"
	"log"
	"strconv"
	"strings"
)

type Operation struct {
	Name  string
	Value int
}

type Computer struct {
	Accumulator int
	Operations  []Operation
	Executed    map[int]bool
}

func (c *Computer) compute(index int) int {
	c.Executed[index] = true

	op := c.Operations[index]

	newIndex := 0

	switch op.Name {
	case "nop":
		newIndex = index + 1

	case "acc":
		newIndex = index + 1
		c.Accumulator += op.Value

	case "jmp":
		newIndex = index + op.Value

	default:
		log.Fatalf("unkown operation %s", op.Name)
	}

	return newIndex
}

func execute(comp *Computer) (int, error) {
	index := 0

	for {
		// loop
		if comp.Executed[index] {
			return comp.Accumulator, errors.New("loop")
		}

		// last execution
		if index == len(comp.Operations) {
			return comp.Accumulator, nil
		}

		index = comp.compute(index)
	}
}

func parse(input io.Reader) []Operation {
	ops := make([]Operation, 0)

	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		line := scanner.Text()

		split := strings.Split(line, " ")
		name := split[0]

		value, err := strconv.Atoi(split[1])
		if err != nil {
			panic(err)
		}

		op := Operation{
			Name:  name,
			Value: value,
		}

		ops = append(ops, op)
	}

	return ops
}

func part1(comp *Computer) int {
	if acc, err := execute(comp); err != nil {
		return acc
	}

	log.Fatal("loop not detected")
	return -1
}

func part2(comp *Computer) int {
	executeChannel := make(chan *Computer, 1)
	finishedChannel := make(chan *Computer, 1)

	go func(exec chan *Computer, finish chan *Computer) {
		for {
			select {
			case c := <-exec:
				if _, err := execute(c); err == nil {
					finish <- c
				}
			}
		}
	}(executeChannel, finishedChannel)

	index := 0
	for {
		select {
		case result := <-finishedChannel:
			return result.Accumulator

		default:
			// keeps the loop alive if the jmp <-> nop conversion is at the end of the
			// computer.Operations list
			if index >= len(comp.Operations) {
				continue
			}

			op := comp.Operations[index]

			if op.Name != "acc" {
				opsCopy := make([]Operation, len(comp.Operations))
				copy(opsCopy, comp.Operations)

				if op.Name == "jmp" {
					opsCopy[index].Name = "nop"
				} else if op.Name == "nop" {
					opsCopy[index].Name = "jmp"
				}

				c := &Computer{
					Operations: opsCopy,
					Executed:   make(map[int]bool),
				}

				executeChannel <- c
			}

			index += 1
		}
	}
}
