package day9

import (
	"bufio"
	"io"
	"sort"
	"strconv"
)

func generateSums(input []int) map[int]bool {
	sums := make(map[int]bool)

	for _, a := range input {
		for _, b := range input {

			if a == b {
				continue
			}

			sums[a+b] = true
		}
	}

	return sums
}

func parse(input io.Reader) []int {
	nums := make([]int, 0)

	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		line := scanner.Text()

		num, err := strconv.Atoi(line)
		if err != nil {
			panic(err)
		}

		nums = append(nums, num)
	}

	return nums
}

func part1(nums []int, preamble int) (int, int) {
	for i, num := range nums[preamble:] {
		end := i + preamble
		sums := generateSums(nums[i:end])

		if !sums[num] {
			return num, i + preamble
		}
	}

	return -1, -1
}

func part2(nums []int, index int) int {
	target := nums[index]

	for i, _ := range nums[:index] {
		seq := make([]int, 0)
		fwd := 0
		for sum(seq) < target {
			seq = append(seq, nums[i+fwd])
			fwd += 1
		}

		if sum(seq) > target {
			continue
		}

		if sum(seq) == target {
			sort.Ints(seq)
			return seq[0] + seq[len(seq)-1]
		}
	}

	return -1
}

func sum(nums []int) int {
	s := 0

	for _, n := range nums {
		s += n
	}

	return s
}
