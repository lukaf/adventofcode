package day1

import (
	"strings"
	"testing"
	"os"

	"github.com/stretchr/testify/assert"
)

func TestPairSum2020(t *testing.T) {
	input := []int{
		1721,
		979,
		366,
		299,
		675,
		1456,
	}

	// 2020 = 1721 + 299
	// 514579 = 1721 * 299
	want := 514579

	result := pairSum2020(input)
	if result != want {
		t.Fatalf("wrong sum, got %v, want %v\n", result, want)
	}
}

func TestTripleSum2020(t *testing.T) {
	input := []int{
		1721,
		979,
		366,
		299,
		675,
		1456,
	}

	// 2020 = 979 + 366 + 675
	// 241861950 = 979 * 366 * 675
	want := 241861950

	result := tripleSum2020(input)
	if result != want {
		t.Fatalf("wrong sum, got %v, want %v\n", result, want)
	}
}

func TestPart1(t *testing.T) {
	want := 902451

	f, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	nums := parse(f)

	assert.Equal(t, want, pairSum2020(nums))
}

func TestPart2(t *testing.T) {
	want := 85555470

	f, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	nums := parse(f)

	assert.Equal(t, want, tripleSum2020(nums))
}

func TestParse(t *testing.T) {
	input := `1
2
3
4
5
6
7
8
`
	want := []int{1, 2, 3, 4, 5, 6, 7, 8}

	f := strings.NewReader(input)
	assert.Equal(t, want, parse(f))
}
